import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'PhotoPage.dart';
import 'UnsplashApi.dart';
import 'main.dart';

class MyHomePageState extends State<HomePage> {
  var gallery = Gallery(); //галерея со списком фото и текущей страницей
  var refreshKey = GlobalKey<RefreshIndicatorState>(); //ключ обновления виджета

  @override
  void initState() {
    super.initState();
    _showToast();
  }
  Future<Null> refreshList() async {
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 2));
    setState(() {});
    return null;
  }

  void _showToast() {
    Fluttertoast.showToast(
        msg: "Потяните вниз для загрузки следующих фото",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        key: refreshKey,
        child: FutureBuilder(
          future: UnsplashApi().getPhotosList(gallery), //асинхронная загрузка списка фото через апи
          builder: (BuildContext context, AsyncSnapshot<Gallery?> snapshot)=>
          snapshot.hasData ? ListView.builder(
            itemCount: snapshot.data!.photos.length,
            itemBuilder: (BuildContext context, int id){
              return ListTile(
                onTap: (){ //событие нажатия на элемент списка
                  Navigator.push(context, MaterialPageRoute(builder: (context) => PhotoPage(snapshot.data!.photos[id])));
                },
                leading: CircleAvatar( //фото в круге
                  backgroundImage: NetworkImage( //загрузка маленького фото
                      snapshot.data!.photos[id].urlPhotoSmall
                  ),
                ),
                title: Text(snapshot.data!.photos[id].username,
                    style: Theme.of(context).textTheme.bodyText2),
                subtitle: Text(snapshot.data!.photos[id].title,
                    style: Theme.of(context).textTheme.bodyText1),
              );
            },
          ) : Center(
            child: CircularProgressIndicator(), //индикатор загрузки
          ),
        ),
        onRefresh: refreshList,
      ),
    );
  }
}