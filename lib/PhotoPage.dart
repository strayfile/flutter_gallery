import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'UnsplashApi.dart';

class PhotoPage extends StatelessWidget {
  final Photo _photo; //текущее фото
  PhotoPage(this._photo);

  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        child: Container(child: LayoutBuilder(
            builder: (context, constraints) =>
            SingleChildScrollView(
              scrollDirection: Axis.horizontal, //горизонтальный скролл растянутого фото
              child: SizedBox(
                  height: constraints.biggest.height,
                  child: FutureBuilder(
                    future: http.get(Uri.parse(this._photo.urlPhotoFull)), //загрузка фото
                    builder: (BuildContext context, AsyncSnapshot<http.Response> snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return Text('Loading failed.');
                        case ConnectionState.active:
                        case ConnectionState.waiting:
                          return CircularProgressIndicator(); //пока фото не загружено
                        case ConnectionState.done:
                          Image image = Image.memory(
                            snapshot.data!.bodyBytes,
                            fit: BoxFit.cover,
                          );
                          return image; //возврат фото
                      }
                    },
                  )
              )
            ),
        )
        ),
      ),
    );
  }
}