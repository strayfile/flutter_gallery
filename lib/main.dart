import 'package:flutter/material.dart';
import 'MyHomePageState.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData( //тема приложения
        scaffoldBackgroundColor: Colors.white,
        backgroundColor: Colors.blueGrey,
        textTheme: const TextTheme(
          bodyText1: TextStyle(fontSize: 12.0), //стиль имени пользователя
          bodyText2: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold), //стиль описания фото
    ),
      ),
      debugShowCheckedModeBanner: false,
      home: HomePage()
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  MyHomePageState createState() => MyHomePageState();
}



