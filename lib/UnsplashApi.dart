import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

//хранит данные о фото и текущую страницу
class Gallery {
  List<Photo> photos = [];
  int _page = 1;
}

//класс с данными о фото
class Photo {
  final String _title;
  final String _username;
  final String _urlPhotoSmall;
  final String _urlPhotoFull;

  Photo(this._title, this._username, this._urlPhotoSmall, this._urlPhotoFull);

  factory Photo.fromJson(Map<String, dynamic> json) { //создание экзмпляра фото из данных json
    return Photo(
        json['alt_description'] ?? "",
        json['user']['name'] ?? "",
        json['urls']['small'] ?? "",
        json['urls']['full'] ?? ""
    );
  }

  String get username => _username;
  String get title => _title;
  String get urlPhotoSmall => _urlPhotoSmall;
  String get urlPhotoFull => _urlPhotoFull;
}

class UnsplashApi {
  final String _url = 'http://api.unsplash.com/photos/';
  final String client = 'client_id=896d4f52c589547b2134bd75ed48742db637fa51810b49b607e37e46ab2c0043';

  //получение данных через апи
  Future<String> _getPhotosFromApi(page) async {
    try {
      final response = await http.get(Uri.parse(_url  + '?page=' + page.toString() + '&per_page=5&'+ client)); //get запрос на страницу с фото по 5шт с токеном пользователя
      if (response.statusCode == 200) { //успешно получено
        return utf8.decode(response.bodyBytes); //перекодировка в utf8
      } else { //ошибка запроса
        throw HttpException("Http error: ${response.statusCode}");
      }
    } catch (e) {
      print(e);
    }
    return "";
  }

//получение данных из json
  Future<Gallery?> getPhotosList(Gallery gallery) async {
    try {
      final String data = await _getPhotosFromApi(gallery._page); //получение данных о фото с сайта
      final dynamic photosJson = json.decode(data); //декодировка строки в json
      List<dynamic> photos = (photosJson as List<dynamic>).map((dynamic el) => Photo.fromJson(el as Map<String, dynamic>)).toList(); //создание списка фото из данных json
      gallery.photos.insertAll(0, (photos as List<Photo>).reversed); //Новые фото вверху
      gallery._page++; //следующая страница
      return gallery;
    } catch (e) {
      print("Couldn't decode json $e");
    }
    return null;
  }
}